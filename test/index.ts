import { expect } from 'chai';
import { config } from 'dotenv';
import { SearchQuery } from '../lib/interfaces/search-query.interface';
import YoutubeApiV3Adapter from '../lib/index';


describe('YoutubeApiV3', () => {
  config();
  const youtubeApiV3 = new YoutubeApiV3Adapter(process.env.API_KEY || 'configure env API_KEY');

  it('should init class with API_KEY', () => {
    expect(youtubeApiV3.getApiKey).to.equal(process.env.API_KEY);
  });

  it('should give 5 suggestions', async () => {
    const suggestions = await youtubeApiV3.getSuggestions('mocha');
    expect(suggestions[0]).to.contain('mocha');
  });

  it('should transform query object in search query string', () => {
    const queryObject: SearchQuery = { q: 'mocha' };
    const queryParams = YoutubeApiV3Adapter.queryToSearchParams(queryObject);
    Object.keys(queryObject).forEach((key) => expect(queryParams).to.contain(key));
    expect(queryParams).to.contain(queryObject.q);
    expect(queryParams).to.contain('&');
    expect(queryParams).to.contain('=');
  });

  // it('should fetch videos', async () => {
  //   const queryObject: SearchQuery = { q: 'mocha' };
  //   const res = await youtubeApiV3.search(queryObject);
  //   expect(res.pageInfo?.totalResults).to.greaterThan(0);
  // });
});
