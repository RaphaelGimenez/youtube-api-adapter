import axios from 'axios';
import jsonp from 'jsonp';
import { CommentsObject } from './interfaces/comments.interface';
import { ChannelsObject } from './interfaces/channels.interface';
import { VideosObject } from './interfaces/videos.interface';
import { SearchQuery, SearchQueryKeys } from './interfaces/search-query.interface';
import { SearchObject } from './interfaces/search.interface';

class YoutubeApiV3Adapter {
    private API_KEY: string;

    private readonly AUTOCOMPLETE_URL: string = 'https://suggestqueries.google.com/complete/search?client=firefox&ds=yt';

    private readonly SEARCH_URL: string = 'https://www.googleapis.com/youtube/v3/search';

    private readonly VIDEOS_URL: string = 'https://www.googleapis.com/youtube/v3/videos';

    private readonly CHANELS_URL: string = 'https://www.googleapis.com/youtube/v3/channels';

    private readonly COMMENTS_URL: string = 'https://www.googleapis.com/youtube/v3/commentThreads';

    constructor(API_KEY: string) {
      if (typeof API_KEY !== 'string' || !API_KEY) {
        throw new Error('ApiKey must be a string');
      }
      this.API_KEY = API_KEY;
      this.SEARCH_URL = this.SEARCH_URL.concat(`?key=${this.API_KEY}&part=snippet&type=video&maxResults=15`);
      this.VIDEOS_URL = this.VIDEOS_URL.concat(`?key=${this.API_KEY}&part=snippet%2CcontentDetails%2Cstatistics&maxResults=15`);
      this.CHANELS_URL = this.CHANELS_URL.concat(`?key=${this.API_KEY}`);
      this.COMMENTS_URL = this.COMMENTS_URL.concat(`?key=${this.API_KEY}&part=snippet%2Creplies`);
    }

    public get getApiKey(): string {
      return this.API_KEY;
    }

    public static queryToSearchParams(query: SearchQuery): string {
      const keys = Object.keys(query) as SearchQueryKeys;
      return keys
        .reduce((accumulator, currentValue) => `${accumulator}&${currentValue}=${query[currentValue]}`, '');
    }

    public getSuggestions(query: string): Promise<string[]> {
      return new Promise<string[]>((resolve, reject) => {
        if (typeof document !== 'undefined') {
          jsonp(
            `${this.AUTOCOMPLETE_URL}&q=${query}&jsonp=suggestCallback`,
            { name: 'suggestCallback' },
            (error: any, data: [string, string[]]) => {
              if (error) {
                reject(new Error('something went wrong while fetching suggestions'));
              }
              if (data) {
                resolve(data[1]);
              }
            },
          );
        } else {
          axios.get(`${this.AUTOCOMPLETE_URL}&q=${query}`, {
            headers: { 'Access-Control-Allow-Origin': '*' },
          })
            .then(({ data }: {data: [string, string[]]}) => {
              resolve(data[1]);
            })
            .catch(() => {
              reject(new Error('something went wrong while fetching suggestions'));
            });
        }
      });
    }

    public search(query: SearchQuery): Promise<SearchObject> {
      return new Promise<SearchObject>((resolve, reject) => {
        axios.get(`${this.SEARCH_URL}${YoutubeApiV3Adapter.queryToSearchParams(query)}`)
          .then(({ data }) => {
            resolve(data);
          })
          .catch(() => {
            reject(new Error('something went wrong while fetching videos'));
          });
      });
    }

    public getVideos(query: SearchQuery): Promise<VideosObject> {
      return new Promise<VideosObject>((resolve, reject) => {
        axios.get(`${this.VIDEOS_URL}${YoutubeApiV3Adapter.queryToSearchParams(query)}`)
          .then(({ data }) => {
            resolve(data);
          })
          .catch(() => {
            reject(new Error('something went wrong while fetching videos'));
          });
      });
    }

    public getChannelById(id: string, details: boolean): Promise<ChannelsObject> {
      const query: SearchQuery = {
        id,
        part: details ? 'snippet%2CcontentDetails%2Cstatistics' : 'snippet',
      };
      return new Promise<ChannelsObject>((resolve, reject) => {
        axios.get(`${this.CHANELS_URL}${YoutubeApiV3Adapter.queryToSearchParams(query)}`)
          .then(({ data }) => {
            resolve(data);
          })
          .catch(() => {
            reject(new Error('something went wrong while fetching the channel'));
          });
      });
    }

    public getComments(query: SearchQuery): Promise<CommentsObject> {
      return new Promise<CommentsObject>((resolve, reject) => {
        axios.get(`${this.COMMENTS_URL}${YoutubeApiV3Adapter.queryToSearchParams(query)}`)
          .then(({ data }) => {
            resolve(data);
          })
          .catch(() => {
            reject(new Error('something went wrong while fetching comments'));
          });
      });
    }
}

export default YoutubeApiV3Adapter;
