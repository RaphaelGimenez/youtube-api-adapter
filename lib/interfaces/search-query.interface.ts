export interface SearchQuery {
    q?: string;
    chart?: string;
    videoCategoryId?: number;
    regionCode?: 'FR';
    id?: string;
    part?: string;
    relatedToVideoId?: string;
    videoId?: string;
}

export type SearchQueryKeys = ['q'];
