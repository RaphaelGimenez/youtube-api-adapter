"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var jsonp_1 = __importDefault(require("jsonp"));
var YoutubeApiV3Adapter = /** @class */ (function () {
    function YoutubeApiV3Adapter(API_KEY) {
        this.AUTOCOMPLETE_URL = 'https://suggestqueries.google.com/complete/search?client=firefox&ds=yt';
        this.SEARCH_URL = 'https://www.googleapis.com/youtube/v3/search';
        this.VIDEOS_URL = 'https://www.googleapis.com/youtube/v3/videos';
        this.CHANELS_URL = 'https://www.googleapis.com/youtube/v3/channels';
        this.COMMENTS_URL = 'https://www.googleapis.com/youtube/v3/commentThreads';
        if (typeof API_KEY !== 'string' || !API_KEY) {
            throw new Error('ApiKey must be a string');
        }
        this.API_KEY = API_KEY;
        this.SEARCH_URL = this.SEARCH_URL.concat("?key=" + this.API_KEY + "&part=snippet&type=video&maxResults=15");
        this.VIDEOS_URL = this.VIDEOS_URL.concat("?key=" + this.API_KEY + "&part=snippet%2CcontentDetails%2Cstatistics&maxResults=15");
        this.CHANELS_URL = this.CHANELS_URL.concat("?key=" + this.API_KEY);
        this.COMMENTS_URL = this.COMMENTS_URL.concat("?key=" + this.API_KEY + "&part=snippet%2Creplies");
    }
    Object.defineProperty(YoutubeApiV3Adapter.prototype, "getApiKey", {
        get: function () {
            return this.API_KEY;
        },
        enumerable: false,
        configurable: true
    });
    YoutubeApiV3Adapter.queryToSearchParams = function (query) {
        var keys = Object.keys(query);
        return keys
            .reduce(function (accumulator, currentValue) { return accumulator + "&" + currentValue + "=" + query[currentValue]; }, '');
    };
    YoutubeApiV3Adapter.prototype.getSuggestions = function (query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (typeof document !== 'undefined') {
                jsonp_1.default(_this.AUTOCOMPLETE_URL + "&q=" + query + "&jsonp=suggestCallback", { name: 'suggestCallback' }, function (error, data) {
                    if (error) {
                        reject(new Error('something went wrong while fetching suggestions'));
                    }
                    if (data) {
                        resolve(data[1]);
                    }
                });
            }
            else {
                axios_1.default.get(_this.AUTOCOMPLETE_URL + "&q=" + query, {
                    headers: { 'Access-Control-Allow-Origin': '*' },
                })
                    .then(function (_a) {
                    var data = _a.data;
                    resolve(data[1]);
                })
                    .catch(function () {
                    reject(new Error('something went wrong while fetching suggestions'));
                });
            }
        });
    };
    YoutubeApiV3Adapter.prototype.search = function (query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            axios_1.default.get("" + _this.SEARCH_URL + YoutubeApiV3Adapter.queryToSearchParams(query))
                .then(function (_a) {
                var data = _a.data;
                resolve(data);
            })
                .catch(function () {
                reject(new Error('something went wrong while fetching videos'));
            });
        });
    };
    YoutubeApiV3Adapter.prototype.getVideos = function (query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            axios_1.default.get("" + _this.VIDEOS_URL + YoutubeApiV3Adapter.queryToSearchParams(query))
                .then(function (_a) {
                var data = _a.data;
                resolve(data);
            })
                .catch(function () {
                reject(new Error('something went wrong while fetching videos'));
            });
        });
    };
    YoutubeApiV3Adapter.prototype.getChannelById = function (id, details) {
        var _this = this;
        var query = {
            id: id,
            part: details ? 'snippet%2CcontentDetails%2Cstatistics' : 'snippet',
        };
        return new Promise(function (resolve, reject) {
            axios_1.default.get("" + _this.CHANELS_URL + YoutubeApiV3Adapter.queryToSearchParams(query))
                .then(function (_a) {
                var data = _a.data;
                resolve(data);
            })
                .catch(function () {
                reject(new Error('something went wrong while fetching the channel'));
            });
        });
    };
    YoutubeApiV3Adapter.prototype.getComments = function (query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            axios_1.default.get("" + _this.COMMENTS_URL + YoutubeApiV3Adapter.queryToSearchParams(query))
                .then(function (_a) {
                var data = _a.data;
                resolve(data);
            })
                .catch(function () {
                reject(new Error('something went wrong while fetching comments'));
            });
        });
    };
    return YoutubeApiV3Adapter;
}());
exports.default = YoutubeApiV3Adapter;
