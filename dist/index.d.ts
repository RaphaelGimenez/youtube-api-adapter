import { CommentsObject } from './interfaces/comments.interface';
import { ChannelsObject } from './interfaces/channels.interface';
import { VideosObject } from './interfaces/videos.interface';
import { SearchQuery } from './interfaces/search-query.interface';
import { SearchObject } from './interfaces/search.interface';
declare class YoutubeApiV3Adapter {
    private API_KEY;
    private readonly AUTOCOMPLETE_URL;
    private readonly SEARCH_URL;
    private readonly VIDEOS_URL;
    private readonly CHANELS_URL;
    private readonly COMMENTS_URL;
    constructor(API_KEY: string);
    get getApiKey(): string;
    static queryToSearchParams(query: SearchQuery): string;
    getSuggestions(query: string): Promise<string[]>;
    search(query: SearchQuery): Promise<SearchObject>;
    getVideos(query: SearchQuery): Promise<VideosObject>;
    getChannelById(id: string, details: boolean): Promise<ChannelsObject>;
    getComments(query: SearchQuery): Promise<CommentsObject>;
}
export default YoutubeApiV3Adapter;
