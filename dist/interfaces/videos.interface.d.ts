export interface Default {
    url: string;
    width: number;
    height: number;
}
export interface Medium {
    url: string;
    width: number;
    height: number;
}
export interface High {
    url: string;
    width: number;
    height: number;
}
export interface Standard {
    url: string;
    width: number;
    height: number;
}
export interface Maxres {
    url: string;
    width: number;
    height: number;
}
export interface Thumbnails {
    default: Default;
    medium: Medium;
    high: High;
    standard: Standard;
    maxres: Maxres;
}
export interface Localized {
    title: string;
    description: string;
}
export interface Snippet {
    publishedAt: Date;
    channelId: string;
    title: string;
    description: string;
    thumbnails: Thumbnails;
    channelTitle: string;
    tags: string[];
    categoryId: string;
    liveBroadcastContent: string;
    localized: Localized;
    defaultAudioLanguage: string;
    defaultLanguage: string;
}
export interface RegionRestriction {
    allowed: string[];
}
export interface ContentDetails {
    duration: string;
    dimension: string;
    definition: string;
    caption: string;
    licensedContent: boolean;
    regionRestriction: RegionRestriction;
    projection: string;
}
export interface Statistics {
    viewCount: string;
    likeCount: string;
    dislikeCount: string;
    favoriteCount: string;
    commentCount: string;
}
export interface Item {
    kind: string;
    etag: string;
    id: string;
    snippet: Snippet;
    contentDetails: ContentDetails;
    statistics: Statistics;
}
export interface PageInfo {
    totalResults: number;
    resultsPerPage: number;
}
export interface VideosObject {
    kind: string;
    etag: string;
    items: Item[];
    nextPageToken: string;
    pageInfo: PageInfo;
}
